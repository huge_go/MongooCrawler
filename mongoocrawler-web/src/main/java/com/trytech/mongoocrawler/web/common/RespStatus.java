package com.trytech.mongoocrawler.web.common;

import com.trytech.mongoocrawler.web.exception.BizException;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年07月15日
 */
public enum RespStatus {
    OK(200, "成功"),
    //#1000-1999 爬虫异常
    CRAWLER_SERVICE_ERROR(1000, "爬虫服务异常"),
    CRAWLER_RUNNING_ERROR(1001, "爬虫已在运行之中"),
    CRAWLER_OCUPPIED_BY_OTHERS_ERROR(1002, "爬虫已被其它用户占用，请稍后再试");
    private int code;
    private String msg;

    RespStatus(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Status toStatus() {
        return new Status(code, msg);
    }

    public BizException toException() {
        return new BizException(code, msg);
    }
}
