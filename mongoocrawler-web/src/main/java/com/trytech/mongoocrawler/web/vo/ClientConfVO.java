package com.trytech.mongoocrawler.web.vo;

import lombok.Data;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年07月01日
 */
@Data
public class ClientConfVO {
    private String ip; //爬虫客户端的ip
    private String destUrl; //爬取网站的url
    private String connectTime; //连接时间
}
