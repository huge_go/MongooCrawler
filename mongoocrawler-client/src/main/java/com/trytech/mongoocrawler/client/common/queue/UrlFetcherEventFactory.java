package com.trytech.mongoocrawler.client.common.queue;

/**
 * Created by hp on 2017-1-25.
 */
public class UrlFetcherEventFactory extends FetcherEventFactory<UrlResultFetcherEvent> {
    @Override
    public UrlResultFetcherEvent newInstance() {
        return new UrlResultFetcherEvent();
    }
}
