package com.trytech.mongoocrawler.client.transport.http;

import com.trytech.mongoocrawler.client.parser.HtmlParser;

/**
 * 爬取url返回的结果
 */
public class UrlResult {
    private String url;
    private HtmlParser parser;
    public UrlResult(String url, HtmlParser parser){
        this.url = url;
        this.parser = parser;
    }

    public HtmlParser getParser() {
        return parser;
    }

    public void setParser(HtmlParser parser) {
        this.parser = parser;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
