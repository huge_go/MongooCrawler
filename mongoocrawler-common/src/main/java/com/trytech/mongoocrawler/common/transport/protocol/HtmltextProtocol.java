package com.trytech.mongoocrawler.common.transport.protocol;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年04月06日
 */
@Data
public class HtmltextProtocol extends AbstractProtocol {

    private String traceId;
    private String url;
    private String data;

    @Override
    public ProtocolType getType() {
        return ProtocolType.DATA;
    }

    @Override
    public String toJSONString() {
        data = data.replaceAll("\"", "'");
        return JSONObject.toJSONString(this);
    }
}
