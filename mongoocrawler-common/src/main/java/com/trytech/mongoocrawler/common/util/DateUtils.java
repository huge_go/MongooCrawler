package com.trytech.mongoocrawler.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年07月01日
 */
public class DateUtils {
    private static SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static String now() {
        return sf.format(new Date());
    }
}
