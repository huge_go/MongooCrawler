package com.trytech.mongoocrawler.server.transport;

import com.trytech.mongoocrawler.common.transport.protocol.CrawlerTransferProtocol;
import com.trytech.mongoocrawler.server.CrawlerContext;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * Created by coliza on 2018/5/1.
 */
public abstract class AbstractNettyServerHandler extends SimpleChannelInboundHandler<CrawlerTransferProtocol> {

    protected CrawlerContext crawlerContext;

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        // 当出现异常就关闭连接
        cause.printStackTrace();
        ctx.close();
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.flush();
    }
}
